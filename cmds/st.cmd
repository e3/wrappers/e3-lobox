# This should be a test startup script
require lobox
require essioc


iocshLoad("${essioc_DIR}/common_config.iocsh", "IOCNAME=test-lobox-ioc")

iocshLoad("$(lobox_DIR)/lobox.iocsh", "DEVICENAME=test-lobox, IPADDR=localhost")
